#include "list.h"

List *listCreate(){
    List *newList = malloc(sizeof(List));
    newList->head = 0;

    return newList;
}

void listPush(List *list, unsigned int value){
    Element *newElement = elementCreate();
    newElement->value = value;

    Element *oldHead = list->head;
    list->head = newElement;
    newElement->pSuccessor = oldHead;
}

Element *listPop(List *list){
    Element *oldHead = list->head;
    list->head = oldHead->pSuccessor;

    return oldHead;
}

void listPrint(List *list){
    if(list->head == 0){ // should compare to NULL or nullpotr
        printf("liste leer\n");
        return;
    }

    for(Element *currentElement = list->head; currentElement != 0; currentElement = currentElement->pSuccessor){
        printf("%i ", currentElement->value);
    }
    printf("\n");
}

void listFillRandom(List* list, int seed, int anzahlWerte, int min,
int max){
    srand(seed);
    for(int i = 0; i < anzahlWerte; i++){
        listPush(list, rand() % (max - min) + min);
    }
}

Element *listFindElement(List *list, unsigned int value){
    for(Element *currentElement = list->head; currentElement != 0; currentElement = currentElement->pSuccessor){
        if(currentElement->value == value){
            return currentElement;
        }
    }
    return 0; // should return NULL or nullptr
}

int listGetIndexOfElement(List *list, unsigned int value){
    int index = 0;
    for(Element *currentElement = list->head; currentElement != 0; currentElement = currentElement->pSuccessor, index++){
        if(currentElement->value == value){
            return index;
        }
    }
    return -1; 
}

Element *listGetElementAtIndex(List *list, unsigned int index){
    int currentIndex = 0;
    for(Element *currentElement = list->head; currentElement != 0; currentElement = currentElement->pSuccessor, currentIndex++){
        if(currentIndex == index){
            return currentElement;
        }
    }
    return 0; // NULL / nullptr 
}

boolean listSwapElements(List *list, unsigned int indexA, unsigned int indexB){
    Element *aElement = listGetElementAtIndex(list, indexA);
    Element *bElement = listGetElementAtIndex(list, indexB);

    if(aElement == 0 || bElement == 0){
        return FALSE;
    }

    Element *aPrecedor = listGetElementAtIndex(list, indexA - 1);
    Element *bPrecedor = listGetElementAtIndex(list, indexB - 1);

    if(aPrecedor != 0){ // NULL / nullptr
        aPrecedor->pSuccessor = bElement;
    }else{
        list->head = bElement;
    }
    if(bPrecedor != 0){ // NULL / nullptr
        bPrecedor->pSuccessor = aElement;
    }else{
        list->head = aElement;
    }

    Element *aSuccedor = aElement->pSuccessor;
    aElement->pSuccessor = bElement->pSuccessor;
    bElement->pSuccessor = aSuccedor;

    return TRUE;
}

boolean deleteElement(List *list, unsigned int searchValue){
    int foundIndex = listGetIndexOfElement(list, searchValue);
    if(foundIndex == -1){
        return FALSE;
    }
    Element *foundElement = listGetElementAtIndex(list, foundIndex);
    if(foundIndex == 0){ // nullptr
        list->head = foundElement->pSuccessor;
    }else{
        Element *precedor = listGetElementAtIndex(list, foundIndex - 1);
        precedor->pSuccessor = foundElement->pSuccessor;
    }
    free(foundElement);

    return TRUE;
}

