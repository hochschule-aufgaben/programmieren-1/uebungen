#include "element.h"

Element *elementCreate(){
    Element *newElement = malloc(sizeof(Element));
    newElement->pSuccessor = 0; // should be NULL or nullptr, no idea where they are defined
    newElement->value = 0;

    return newElement;
}