#include "test.h"

void test_pushPrint(){
    List *list = listCreate();

    listPrint(list);
    for(unsigned int i = 1; i <= 9; i++){
        listPush(list, i);
    }

    listPrint(list);
}

void testListFindElement(){
    List *list = listCreate();
    if(listFindElement(list, 0) == NULL){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    for(unsigned int i = 1; i <= 10; i++){
        listPush(list, i);
    }
    
    if(listFindElement(list, 5)->value == 5){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    if(listFindElement(list, 20) == NULL){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }
}

void testListGetIndexOfElement(void){
    List *list = listCreate();
    if(listGetIndexOfElement(list, 0) == -1){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    for(unsigned int i = 1; i <= 10; i++){
        listPush(list, i);
    }

    listPrint(list);

    // wrong result since list is reversed due to 
    // elements being inserted at beginning
    if(listGetIndexOfElement(list, 1) == 0){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    if(listGetIndexOfElement(list, 4) == 3){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    if(listGetIndexOfElement(list, 25) == -1){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }
}

void testListGetElementAtIndex(void){
    List *list = listCreate();

    if(listGetElementAtIndex(list, 5) == NULL){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }

    for(unsigned int i = 1; i <= 10; i++){
        listPush(list, i);
    }


    if(listGetElementAtIndex(list, 5)->value == 6){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }


    if(listGetElementAtIndex(list, 15) == NULL){
        printf("Korrekt!\n");
    }else{
        printf("Falsch!\n");
    }
}

int main(){
    testListGetElementAtIndex();
}