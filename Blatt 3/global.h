#pragma once
#include <stdio.h>
#include <stdlib.h>
// Makros
#define MATRIXSIZE 10
#define ARRAYSIZE 200
#define CHAR 0
#define VALUE 1
#define SEED 5
/* Aufgabe – Printausgaben */
void printArray(const char *array, int length)
{
    for (int i = 0; i < length - 1; i++)
    {
        printf("%i, ", array[i]);
    }
    if (length == 0)
        return;
    printf("%i", array[length - 1]);
}

void printMatrix(const char matrix[MATRIXSIZE][MATRIXSIZE], int length, int type)
{
    for (int y = 0; y < length; y++)
    {
        for (int x = 0; x < length; x++)
        {
            const char value = matrix[x][y];
            if (type == VALUE)
            {
                printf("%c ", value);
            }
            else if (type == CHAR)
            {
                printf("%2i ", value);
            }
            printf("\n");
        }
    }
}
/* Aufgabe – Zufallszahlen */
unsigned char myRand(int max)
{
    return rand() % max;
}
void randArray(char *array, int length, int max)
{
    for (int i = 0; i < length; i++)
    {
        array[i] = myRand(max);
    }
}
void randMatrix(char matrix[MATRIXSIZE][MATRIXSIZE], int length, int max)
{
    for (int y = 0; y - length; y++)
    {
        for (int x = 0; x < length; x++)
        {
            matrix[x][y] = myRand(max);
        }
    }
}
int minArray(const int *array, int length)
{
    int currentMin = array[0];
    for (int i = 1; i < length; i++)
    {
        if (array[i] < currentMin)
        {
            currentMin = array[i];
        }
    }
    return currentMin;
}
int maxArray(const int *array, int length)
{
    int currentMax = array[0];
    for (int i = 1; i < length; i++)
    {
        if (array[i] > currentMax)
        {
            currentMax = array[i];
        }
    }
    return currentMax;
}
void countArray(const int *array, int length, int occurrence[256])
{
    for(int i = 0; i < 256; i++){
        occurrence[i] = 0;
    }
    for (int i = 0; i < length; i++)
    {
        if(array[i] >= 256){
            continue;
        }
        occurrence[array[i]]++;
    }
}
void countingSortArray(int *array, int length)
{
    int occurences[256];
    countArray(array, length, occurences);
    int index = 0;
    for(int currentNumber = 0; currentNumber < 256; currentNumber++){
        for(int i = 0; i < occurences[currentNumber]; i++, index++){
            array[index] = currentNumber;
        }
    }
}
int interpol(int value, int minValue, int maxValue, int steps){
    return (value - minValue) / ((maxValue - minValue) / steps);
}
/* Aufgabe - Wortspiel */
void printHistogramm(const int *array, int length)
{
    int maxValue = maxArray(array, length);
    int occurence[256];

    countArray(array, length, occurence);
    float maxOccurence = maxArray(occurence, 256);
    
    for (int y = 10; y > 0; y--)
    {
        float currentHeight = y / 10.0;
        for (int x = 0; x <= maxValue; x++)
        {
            int currentValue = occurence[x];
            float progress = currentValue / maxOccurence;
            if(progress >= currentHeight){
                printf("***");
            }else{
                printf("   ");
            }
        }
        printf("\n");
    }
    for(int x = 0; x <= maxValue; x++){
        printf("%3i", x);
    }
}
void printPositive(const int *array, int length)
{
    int max = maxArray(array, length);
    int occurences[256];
    countArray(array, length, occurences);
    for (int i = 0; i <= max; i++)
    {
        printf("Wert %i:  Häufigkeit %i\n", i, occurences[i]);
    }
}
/* Aufgabe - Wortspiel */
void setArray(char *array, int length, char value)
{
    for (int i = 0; i < length; i++)
    {
        array[i] = value;
    }
}

void readInputAsString(char *array, int length)
{
    for(int i = 0; i < length; i++){
        scanf("%c", array + i);
    }
}

char isSingleUpperCase(char value)
{
    return value >= 'A' && value <= 'Z';
}
char isSingleLowerCase(char value)
{
    return value >= 'a' && value <= 'z';
}
char isUpperCase(const char *array, int index)
{
    return isSingleUpperCase(array[index]);
}
char isLowerCase(const char *array, int index)
{
    return isSingleLowerCase(array[index]);
}

void toUpperCase(char *array, int length)
{
    for (int i = 0; i < length; i++)
    {
        if (isLowerCase(array, i))
        {
            array[i] -= 'A' - 'a';
        }
    }
}
void countUpperCase(char *array, int length)
{
    int uppercaseCount = 0;
    for (int i = 0; i < length; i++)
    {
        if (isUpperCase(array, i))
        {
            uppercaseCount++;
        }
    }

    printf("Die ersten %i zeichen enthalten %i Großbuchstaben\n", length, uppercaseCount);
}
void toLowerCase(char *array, int length)
{
    for (int i = 0; i < length; i++)
    {
        if (isUpperCase(array, i))
        {
            array[i] += 'A' - 'a';
        }
    }
}
void countLowerCase(char *array, int length)
{
    int lowercaseCount = 0;
    for (int i = 0; i < length; i++)
    {
        if (isLowerCase(array, i))
        {
            lowercaseCount++;
        }
    }

    printf("Die ersten %i zeichen enthalten %i Kleinbuchstaben\n", length, lowercaseCount);
}

void caesarChiffre(const char *array, int length, int shift)
{
    const int alphabetLength = 'z' - 'a' + 1;
    char encrypted[ARRAYSIZE];
    for (int i = 0; i < length; i++)
    {
        if(!isLowerCase(array, i) && !isUpperCase(array, i)){
            encrypted[i] = array[i];
            continue;
        }
        encrypted[i] = array[i] + shift;

        while((isLowerCase(array, i) && !isLowerCase(encrypted, i)) || (isUpperCase(array, i) && !isUpperCase(encrypted, i))){
            encrypted[i] -= alphabetLength;
        }
    }
    printf("Original text: %.*s\n", length, array);
    printf("Encrypted text: %.*s\n", length, encrypted);
}
// Aufgabe - Sudoku Test
char lineTest(const char matrix[MATRIXSIZE][MATRIXSIZE], int line, int length)
{
/* IHR CODE */ }
char columnTest(const char matrix[MATRIXSIZE][MATRIXSIZE], int column, int length)
{ /* IHR CODE */
}
char sudokuTest(const char matrix[MATRIXSIZE][MATRIXSIZE], int length)
{ /* IHR CODE */
}
