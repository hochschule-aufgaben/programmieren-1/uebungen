#include "global.h"

void testCeasar(){
    caesarChiffre("Zwei!", 5, 2);
}

void testPrintPositive(){
    int myArray[] = { 1, 2, 3, 0, 1, 4, 1, 0, 2, 1, 0, 3, 3 };
    printPositive(myArray, sizeof(myArray) / sizeof(myArray[0]));
}

void testHistogram(){
    int myArray[] = {0, 1, 0, 3, 0, 3, 6, 3};
    printHistogramm(myArray, 8);
}

int main()
{
    testHistogram();

    return 0;
}