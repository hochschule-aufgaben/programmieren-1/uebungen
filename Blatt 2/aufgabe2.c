#include <stdio.h>

void sanduhr(unsigned int width, char filler){
    int halfHeight = width / 2;
    int halfWidth = width / 2;
    if(width % 2) halfWidth++;

    // print upper edge
    for(int i = 0; i < width; i++){
        putchar(filler);
    }

    // print hollow reversed pyramid
    putchar('\n');
    for(int y = 1; y < halfHeight; y++){
        for(int x = 0; x < y; x++){
            putchar(' ');
        }
        putchar(filler);
        for(int x = width - (2 * y); x > 2; x--){
            putchar(' ');
        }
        putchar(filler);
        putchar('\n');
    }

    // print center single char, if width is odd
    if(width % 2){
        for(int i = 0; i < width / 2; i++){
            putchar(' ');
        }
        putchar(filler);
        putchar('\n');
    }

    // print lower, proper, filled pyramid
    for(int y = 0; y < halfHeight; y++){
        for(int x = 0; x < (width / 2) - y - 1; x++){
            putchar(' ');
        }
        for(int x = 0; x < (2 * y) + 2 + width % 2; x++){
            putchar(filler);
        }
        putchar('\n');
    }
    putchar('\n');
}

int main() {
    sanduhr(7, '*');
    return 0;
}
