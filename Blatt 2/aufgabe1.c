#include <stdio.h>

void rechteck(unsigned int width, unsigned int height, char filler){
    for(int x = 0; x < width; x++) putchar(filler);
    putchar('\n');
    for(int y = 2; y < height; y++){
        putchar(filler);
        for(int x = 2; x < width; x++){
            putchar(' ');
        }
        putchar(filler);
        putchar('\n');
    }
    for(int x = 0; x < width; x++) putchar(filler);
}

int main(){
    rechteck(4, 6, 'x');
    return 0;
}