#include<stdio.h>

int calcSumIntArray(int s1, int s2, int s3, int s4, int s5)
{
    return s1 + s2 + s3 + s4 + s5;
}

int main(void)
{
    int samples1 = 2;
    int samples2 = 6;
    int samples3 = 8;
    int samples4 = 10;
    int samples5 = 4;
    int summe = calcSumIntArray(samples1, samples2, samples3, samples4, samples5);
    printf("Die Summe ist: = %d\n", summe);
    return 0;
}