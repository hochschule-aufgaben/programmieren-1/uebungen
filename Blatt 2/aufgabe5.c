#include<stdio.h>
#include<stdlib.h>

int wuerfel6(){
    return rand() % 6 + 1;
}

char dreier(int w1, int w2, int w3, int w4, int w5){
    for(int goal = 1; goal <= 6; goal++){
        int goalSum = 0;
        if(w1 == goal) goalSum++;
        if(w2 == goal) goalSum++;
        if(w3 == goal) goalSum++;
        if(w4 == goal) goalSum++;
        if(w5 == goal) goalSum++;
        if(goalSum == 3){
            return 1;
        }
    }
    return 0;
}

char strasse(int w1, int w2, int w3, int w4, int w5){
    int counts[] = {0, 0, 0, 0, 0, 0};
    counts[w1 - 1]++;
    counts[w2 - 1]++;
    counts[w3 - 1]++;
    counts[w4 - 1]++;
    counts[w5 - 1]++;

    return (counts[1] == 1 && counts[2] == 1 && counts[3] == 1 && counts[4] == 1) && (counts[0] == 1 || counts[5] == 1);
}


void wuerfe(void){
    int tries = 0;
    int w1, w2, w3, w4, w5;
    do{
        tries++;
        w1 = wuerfel6();
        w2 = wuerfel6();
        w3 = wuerfel6();
        w4 = wuerfel6();
        w5 = wuerfel6();
    }while(!strasse(w1, w2, w3, w4, w5));
    printf("Anzahl der Würfe: %i\n", tries);
}

int main(void){
    srand(7); //Wird zur Initialisierung der Zufallszahlen benoetigt
    //Aufgabe 7
    wuerfe();
    return 0;
}