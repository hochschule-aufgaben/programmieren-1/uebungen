#include<stdio.h>

int ggT(int a, int b){
    int denominator = a < b ? a : b;
    while((a % denominator != 0) || (b % denominator != 0)){
        denominator--;
    }
    return denominator;
}

void bruchMultiplikation(int zaehler1, int nenner1, int zaehler2, int nenner2){
    int multipliedCounter = zaehler1 * zaehler2;
    int multipliedDenominator = nenner1 * nenner2;

    int ggt = ggT(multipliedCounter, multipliedDenominator);

    printf("%i   %i     %i   %i\n", zaehler1, zaehler2, multipliedCounter, multipliedCounter / ggt);
    printf("- * -  =  --   --\n");
    printf("%i   %i     %i   %i\n", nenner1, nenner2, multipliedDenominator, multipliedDenominator / ggt);
}

int main(void){
    //Aufgabe 5
    int zaehler1 = 5, nenner1 = 8, zaehler2 = 2, nenner2 = 75;
    bruchMultiplikation(zaehler1, nenner1, zaehler2, nenner2); 
    return 0;
}