#include <stdio.h>

int main(){
    int stundenA = 12, minutenA = 20, sekundenA = 53; //Kurzschreibweise
    int stundenB = 18, minutenB = 48, sekundenB = 5; //Kurzschreibweise

    int secondsTotalA = stundenA * 3600 + minutenA * 60 + sekundenA;
    int secondsTotalB = stundenB * 3600 + minutenB * 60 + sekundenB;

    int secondsDif = secondsTotalB - secondsTotalA;

    printf("seconds delta: %i\n", secondsDif);
}