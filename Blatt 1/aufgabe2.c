#include <stdio.h>

int main(){
    float weight = 58, size = 180, age = 25;
    float G = 66.47 + (13.7 * weight) + (5 * size) - (6.8 * age);
    printf("G (Male) %f\n", G);

    weight = 90;
    size = 160;
    age = 45;

    G = 655.1 + (9.6 * weight) + (1.8 * size) - (4.7 * age);
    printf("G (Female) %f\n", G);
}