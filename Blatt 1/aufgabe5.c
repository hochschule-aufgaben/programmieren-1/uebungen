#include <stdio.h>

int main(){
    int epochSeconds = 24060;
    int stunden = epochSeconds / 3600;
    int minuten = epochSeconds / 60 % 60;
    int sekunden = epochSeconds % 60;

    printf("%02i:%02i:%02i\n", stunden, minuten, sekunden);
}

void teilB(){
    int epochSeconds = 24060;

    printf("%02i:%02i:%02i\n", epochSeconds / 3600, epochSeconds / 60 % 60, epochSeconds % 60);
}